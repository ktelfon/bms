package lv.sda.bms.db.repos;

import lv.sda.bms.model.Author;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

import static lv.sda.bms.db.SessionManager.getSessionFactory;

public class AuthorRepo {

    public boolean save(Author author) {
        Integer result = null;
        Session session = getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        try {
            result = (Integer) session.save(author);
            transaction.commit();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            transaction.rollback();
            return false;
        }
    }

    public List<Author> getAllAuthors() {
        List<Author> result = null;
        Session session = getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        try {
            CriteriaQuery<Author> query = session.getCriteriaBuilder().createQuery(Author.class);
            result = session.createQuery(query.select(query.from(Author.class))).list();
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            transaction.rollback();
        }
        return result;
    }

    public boolean delete(String firstName, String lastName) {
        return command(Command.DELETE, firstName, lastName);
    }

    public boolean update(String firstName, String lastName) {
        return command(Command.UPDATE, firstName, lastName);
    }

    private boolean command(Command command, String firstName, String lastName) {
        Session session = getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        try {
            // your code
            String hql = command + " from Author where firstname= :firstname AND lastname= :lastname";
            Query query = session.createQuery(hql);
            query.setParameter("firstname", firstName);
            query.setParameter("lastname", lastName);
            System.out.println(query.executeUpdate());
            transaction.commit();
            return true;
        } catch (Throwable t) {
            transaction.rollback();
            t.printStackTrace();
            return false;
        }
    }

    public boolean delete(Author author) {
        Session session = getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        try {
            // your code
            session.delete(author);
            transaction.commit();
            return true;
        } catch (Throwable t) {
            transaction.rollback();
            t.printStackTrace();
            return false;
        }
    }
}
