package lv.sda.bms.db.repos;

public enum Command {
    UPDATE("update"),DELETE("delete");

    private String value;
    Command(String value) {
        this.value = value;
    }
}
