package lv.sda.bms.service;

import lv.sda.bms.db.repos.AuthorRepo;
import lv.sda.bms.model.Author;

import java.util.List;
import java.util.stream.Collectors;

public class AuthorService {

    public static final String VALIDATION_FAILED_MSG = "Empty names";
    public static final String SAVED_MSG = "Saved";
    public static final String NOT_SAVED_MSG = "Not Saved";

    private AuthorRepo authorRepo;

    public AuthorService(AuthorRepo authorRepo) {
        this.authorRepo = authorRepo;
    }

    public String save(String firstName, String lastName) {

        if ("".equals(firstName) || "".equals(lastName)) {
            return VALIDATION_FAILED_MSG;
        }
        Author author = new Author();
        author.setFirstname(firstName);
        author.setLastname(lastName);
        return authorRepo.save(author) ? SAVED_MSG : NOT_SAVED_MSG;
    }

    public List<String> getAllAuthorsNames() {
        return authorRepo.getAllAuthors()
                .stream()
                .map(a -> a.getId() + " " + a.getFirstname() + " " + a.getLastname())
                .collect(Collectors.toList());

    }

    public boolean delete(String firstName, String lastName){
        boolean delete = authorRepo.delete(firstName, lastName);
        return delete;
    }

    public boolean update(String firstName, String lastName) {
        boolean update = authorRepo.update(firstName, lastName);
        return update;
    }

    public boolean delete(int id) {
        Author toDelete = new Author();
        toDelete.setId(id);
        return authorRepo.delete(toDelete);
    }
}
