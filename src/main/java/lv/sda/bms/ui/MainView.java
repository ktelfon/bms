package lv.sda.bms.ui;

import lv.sda.bms.ui.author.AuthorView;

import javax.swing.*;

public class MainView {

    private boolean showAuthorView = true;
    private AuthorView authorView;

    public MainView(AuthorView authorView) {
        this.authorView = authorView;
    }

    public void show(){
        JFrame frame = new JFrame();

        JPanel mainPanel = new JPanel();
        JButton showAuthorViewButton = new JButton("Add author");
        showAuthorViewButton.addActionListener(e->{
            showAuthorView = !showAuthorView;
            authorView.getMainPanel().setVisible(showAuthorView);
        });
        mainPanel.add(showAuthorViewButton);
        mainPanel.add(authorView.getMainPanel());


        frame.add(mainPanel);

        frame.setSize(1300, 300);

        frame.show();
    }
}
