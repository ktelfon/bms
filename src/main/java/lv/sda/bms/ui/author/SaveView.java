package lv.sda.bms.ui.author;

import lv.sda.bms.ui.Util;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.List;

public class SaveView {

    private final int PANEL_WIDTH = 1300;
    private final int PANEL_HEIGHT = 300;

    private final ActionListener saveButtonActionListener;
    private JPanel mainPanel;
    private JLabel firstNameLabel;
    private JLabel lastNameLabel;
    private JLabel saveResultLabel;
    private JTextField lastNameTextField;
    private JTextField firstNameTextField;
    private JButton saveButton;

    public SaveView(ActionListener saveButtonActionListener) {
        this.saveButtonActionListener = saveButtonActionListener;

        generate();
    }


    private void generate() {
        mainPanel = new JPanel();
        firstNameLabel = new JLabel("First Name");
        lastNameLabel = new JLabel("Last Name");
        saveResultLabel = new JLabel("");

        firstNameTextField = new JTextField("", 20);
        lastNameTextField = new JTextField("", 20);

        saveButton = new JButton("Save");


        saveButton.addActionListener(saveButtonActionListener);

        Border blackline = BorderFactory.createLineBorder(Color.black);
        mainPanel.setBorder(blackline);
        mainPanel.setSize(PANEL_WIDTH, PANEL_HEIGHT);

        GridBagLayout layout = new GridBagLayout();

        mainPanel.setLayout(layout);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.HORIZONTAL;
        Util.addComponentToPanel(firstNameLabel, mainPanel, gbc, 0, 0);
        Util.addComponentToPanel(firstNameTextField, mainPanel, gbc, 1, 0);
        Util.addComponentToPanel(lastNameLabel, mainPanel, gbc, 0, 1);
        Util.addComponentToPanel(lastNameTextField, mainPanel, gbc, 1, 1);
        Util.addComponentToPanel(saveButton, mainPanel, gbc, 0, 2);
        Util.addComponentToPanel(saveResultLabel, mainPanel, gbc, 1, 2);
    }

    public void setSaveResult(String result) {
        saveResultLabel.setText(result);
        Thread t = new Thread(() -> {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            saveResultLabel.setText("");
        });
        t.start();
    }

    public String getFirstName() {
        return firstNameTextField.getText();
    }

    public String getLastName() {
        return lastNameTextField.getText();
    }

    public List<JTextField> getTextFields() {
        return List.of(firstNameTextField, lastNameTextField);
    }

    public Component getPanel() {

        return mainPanel;
    }
}
