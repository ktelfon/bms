package lv.sda.bms.ui.author;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class UpdateView {

    private JPanel mainPanel;
    private JLabel firstName;
    private JLabel lastName;
    private JTextField fistNameTextField;
    private JTextField lastNameTextField;
    private JButton update;
    private ActionListener updateButtonListener;

    public UpdateView(ActionListener listener) {
        updateButtonListener = listener;
        generate();
    }


    private void generate() {
        mainPanel = new JPanel();
        java.util.List<JComponent> components = new ArrayList<>();
        firstName = new JLabel("");
        components.add(firstName);
        lastName = new JLabel("");
        components.add(lastName);
        fistNameTextField = new JTextField(20);
        components.add(fistNameTextField);
        lastNameTextField = new JTextField(20);
        components.add(lastNameTextField);
        update = new JButton("Update");
        mainPanel.add(update);
        update.addActionListener(updateButtonListener);
        components.forEach(mainPanel::add);
    }

    public String getFirstName() {
        return fistNameTextField.getText();
    }

    public String getLastName() {
        return lastNameTextField.getText();
    }

    public Component getPanel() {
        return mainPanel;
    }
}
