package lv.sda.bms.ui.author;

import lv.sda.bms.service.AuthorService;
import lv.sda.bms.ui.Util;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class AuthorView {

    private final int PANEL_WIDTH = 1300;
    private final int PANEL_HEIGHT = 300;

    int selectedAuthorId;

    private JPanel mainPanel;
    private SaveView saveView;
    private UpdateView updateView;
    private ListView listView;

    private DefaultListModel lm;

    private AuthorService authorService;

    public AuthorView(AuthorService authorService) {
        this.authorService = authorService;

        lm = new DefaultListModel();

        generateMainPanel();
    }

    private void generateMainPanel() {

        mainPanel = new JPanel();
        listView = new ListView(lm, getDeleteButtonActionListener());
        listView.setMouseListener(authorListMouseListener(lm, listView.getList()));
        saveView = new SaveView(getSaveButtonActionListener());
        updateView = new UpdateView(getActionListener());

        mainPanel.add(listView.getPanel());
        mainPanel.add(saveView.getPanel());
        mainPanel.add(updateView.getPanel());
    }

    private ActionListener getActionListener() {
        return a -> {
            authorService.update(updateView.getFirstName(), updateView.getLastName());

            listView.refreshAuthorList(authorService.getAllAuthorsNames());
        };
    }

    private ActionListener getSaveButtonActionListener() {
        return actionEvent -> {

            String result = authorService.save(saveView.getFirstName(), saveView.getLastName());
            listView.refreshAuthorList(authorService.getAllAuthorsNames());
            saveView.setSaveResult(result);
            Util.clearFields(saveView.getTextFields());
        };
    }

    private ActionListener getDeleteButtonActionListener() {
        return actionEvent -> {
            authorService.delete(selectedAuthorId);
            listView.refreshAuthorList(authorService.getAllAuthorsNames());
        };
    }

    private MouseListener authorListMouseListener(DefaultListModel lm, JList authorList) {
        return new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                String o = (String) lm.get(authorList.getSelectedIndex());
                String[] s = o.split(" ");
                selectedAuthorId = Integer.parseInt(s[0]);
            }


            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        };
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }
}
