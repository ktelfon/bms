package lv.sda.bms.ui.author;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.util.List;

public class ListView {

    private JPanel mainPanel;
    private JList authorList;
    private JButton deleteButton;
    private DefaultListModel lm;
    private MouseListener mouseListener;
    private final ActionListener deleteButtonActionListener;

    public ListView(DefaultListModel lm, ActionListener deleteButtonActionListener) {
        this.lm = lm;
        this.deleteButtonActionListener = deleteButtonActionListener;
        generate(lm, deleteButtonActionListener);
    }

    private void generate(DefaultListModel lm, ActionListener deleteButtonActionListener) {
        authorList = new JList(lm);
        mainPanel = new JPanel();
        deleteButton = new JButton("Delete");
        deleteButton.addActionListener(deleteButtonActionListener);
        mainPanel.add(authorList);
        mainPanel.add(deleteButton);
    }

    public void refreshAuthorList(List<String> allAuthorsNames) {
        lm.clear();
        lm.addAll(allAuthorsNames);
    }

    public JList getList() {
        return authorList;
    }

    public Component getPanel() {
        return mainPanel;
    }

    public void setMouseListener(MouseListener authorListMouseListener) {
        this.mouseListener = authorListMouseListener;
        authorList.addMouseListener(mouseListener);
    }
}
