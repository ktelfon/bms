package lv.sda.bms.ui;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class Util {

    public static void clearFields(List<JTextField> textFields) {
        textFields.forEach(tf->{
            tf.setText("");
        });
    }

    public static void addComponentToPanel(JComponent component, JPanel p, GridBagConstraints gbc, int x, int y) {
        gbc.gridx = x;
        gbc.gridy = y;
        p.add(component, gbc);
    }
}
