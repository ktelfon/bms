package lv.sda.bms;

import lv.sda.bms.db.repos.AuthorRepo;
import lv.sda.bms.service.AuthorService;
import lv.sda.bms.ui.author.AuthorView;
import lv.sda.bms.ui.MainView;

public class BookManagementSystem {

    private MainView mainView;
    private AuthorView authorView;
    private AuthorService authorService;

    BookManagementSystem(){
        authorService = new AuthorService(new AuthorRepo());
        authorView = new AuthorView(authorService);


        mainView = new MainView(authorView);


        show();

    }

    private void show() {
        mainView.show();
    }
}
