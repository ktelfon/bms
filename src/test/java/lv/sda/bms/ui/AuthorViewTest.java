package lv.sda.bms.ui;

import lv.sda.bms.service.AuthorService;
import lv.sda.bms.ui.author.AuthorView;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class AuthorViewTest {

    private AuthorView authorView;

    @BeforeEach
    void setUp() {

//        AuthorRepo repo = mock(AuthorRepo.class);
//        AuthorService authorService = new AuthorService(repo);
        AuthorService mockService = mock(AuthorService.class);
        authorView = new AuthorView(mockService);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getMainPanel() {
        assertEquals(true,authorView.getMainPanel() != null);
        assertEquals(1300,authorView.getMainPanel().getSize().width);
        assertEquals(300,authorView.getMainPanel().getSize().height);
    }
}