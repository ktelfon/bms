package lv.sda.bms.service;

import lv.sda.bms.db.repos.AuthorRepo;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class AuthorServiceTest {

    private AuthorService authorService;

    @BeforeEach
    void setUp() {
        AuthorRepo mockRepo = mock(AuthorRepo.class);
        when(mockRepo.save(any())).thenReturn(true);
        authorService = new AuthorService(mockRepo);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void save() {
        assertEquals(AuthorService.VALIDATION_FAILED_MSG,authorService.save("",""));
        assertEquals("Saved",authorService.save("s","3"));
    }
}